<?php

namespace App\Http\Controllers;

use App\Classes\DataManager;
use Illuminate\Http\Request;
//use App\Classes\Request as MyRequest;
use App\Classes\Response as MyResponse;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;

class AjaxSelectController extends Controller
{

//    private $objRequest;
    private $objResponse;
    private $DB;

    public function __construct(Request $request)
    {

//        $this->objRequest = new MyRequest();
        $this->objResponse = new MyResponse();
        $validator = Validator::make($request->all(), [
            'domain' => 'required'
        ]);

        if (!$validator->fails())
            $this->DB = setConnection($request->all()['domain']);
    }

    public function getSelectTables()
    {
        if (!isset($this->DB))
            return $this->objResponse->Response(false, __FILE__, __LINE__);

        $tables = $this->DB->select('SHOW TABLES');

        foreach ($tables as $table)
            foreach ($table as $key => $value)
                $data[] = $value;

        sort($data);

        return $this->objResponse->Response(true, __FILE__, __LINE__, null, $data);
    }

    public function getSelectColumns(Request $request)
    {

        if (!isset($this->DB))
            return $this->objResponse->Response(false, __FILE__, __LINE__);

        if (getGuestConnection()) {
            $data = Schema::connection(getGuestConnection())->getColumnListing($request->all()['table']);
            return $this->objResponse->Response(true, __FILE__, __LINE__, null, $data);
        }

        return $this->objResponse->Response(true, __FILE__, __LINE__, null, ['error! there is no connection']);
    }

    public function getSelectOperators()
    {

        if (!isset($this->DB))
            return $this->objResponse->Response(false, __FILE__, __LINE__);

        return $this->objResponse->Response(true, __FILE__, __LINE__, null, DataManager::getOperators());
    }
}
