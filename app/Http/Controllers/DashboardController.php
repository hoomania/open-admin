<?php

namespace App\Http\Controllers;

use App\Sites;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    public function Index()
    {
        $objSites = new Sites();

        return view('dashboard', [
            'domains' => $objSites->IndexDomains()
        ]);
    }
}
