<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\Request as MyRequest;
use App\Classes\Response as MyResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AjaxLoginController extends Controller
{

    private $objRequest;
    private $objResponse;

    public function __construct()
    {

        $this->objRequest = new MyRequest();
        $this->objResponse = new MyResponse();
    }

    public function Login(Request $request)
    {

        $data = [
            'email' => $request->all()['username'],
            'password' => base64_decode($request->all()['password']),
        ];

        $validator = Validator::make($data, [
            'email' => 'required|min:4|max:32',
            'password' => 'required|min:4|max:32',
        ]);

        if ($validator->fails()) {

            $message = $this->objResponse->Message('msg', '<p>Wrong Data!</p>');
            return $this->objResponse->Response(false, __FILE__, __LINE__, $message, null);
        }

        $auth = Auth::attempt($data);

        if (!$auth){

            $message = $this->objResponse->Message('msg', '<p>Amazing! something is wrong.</p>');
            return $this->objResponse->Response(false, __FILE__, __LINE__, $message, null);
        }

        return $this->objResponse->Response(true, __FILE__, __LINE__, null, '/admin/dashboard');
    }
}
