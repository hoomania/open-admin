<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Auth;
use App\Sites;

class LoginController extends Controller
{

    public function Index()
    {
        if (Auth::check()) {

            $objSites = new Sites();

            return view('dashboard', [
                'domains' => $objSites->IndexDomains()
            ]);
        }

        return view('login');
    }

    public function Logout()
    {
        Auth::logout();

        return view('login');
    }
}
