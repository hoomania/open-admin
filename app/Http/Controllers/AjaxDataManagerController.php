<?php

namespace App\Http\Controllers;

use App\Classes\DataManager;
use App\Sites;
use Illuminate\Http\Request;
use App\Classes\Request as MyRequest;
use App\Classes\Response as MyResponse;
use Illuminate\Support\Facades\Validator;

class AjaxDataManagerController extends Controller
{
    private $objRequest;
    private $objResponse;
    private $objDataManager;

    public function __construct(Request $request)
    {

        $this->objRequest = new MyRequest();
        $this->objResponse = new MyResponse();

        $validator = Validator::make($request->all(), [
            'domain' => 'required',
            'table' => 'required',
        ]);

        if (!$validator->fails())
            $this->objDataManager = new DataManager($request->all()['domain'], $request->all()['table']);
    }

    public function getTable()
    {

        if (!isset($this->objDataManager))
            return $this->objResponse->Response(false, __FILE__, __LINE__);

        return $this->objResponse->Response(true, __FILE__, __LINE__, null, $this->objDataManager->TableBody());
    }

    public function getColumnContent(Request $request)
    {

        if (!isset($this->objDataManager))
            return $this->objResponse->Response(false, __FILE__, __LINE__);

        $column = $request->all()['column'];
        $data = $this->objDataManager->Read('id', '=', $request->all()['id']);
        $data = json_decode(json_encode($data), True)[0][$column];

        $data = [
            'content' => $data,
            'identity' => [
                'domain' => $request->all()['domain'],
                'table' => $request->all()['table'],
                'id' => $request->all()['id'],
                'column' => $request->all()['column'],
            ]
        ];

        return $this->objResponse->Response(true, __FILE__, __LINE__, null, $data);
    }

    public function FormMaker(Request $request)
    {

        if (!isset($this->objDataManager))
            return $this->objResponse->Response(false, __FILE__, __LINE__);

        $objSites = new Sites();
        $database = $objSites->getDatabaseInfo($request->all()['domain'])['database'];
        $table = $request->all()['table'];
        $data = $this->objDataManager->FormMaker($database, $table);
        return $this->objResponse->Response(true, __FILE__, __LINE__, null, $data);
    }

    public function Read(Request $request)
    {

        if (!isset($this->objDataManager))
            return $this->objResponse->Response(false, __FILE__, __LINE__);

        $validator = Validator::make($request->all(), [
            'column' => 'nullable',
            'operator' => 'nullable',
            'value' => 'nullable',
        ]);

        if ($validator->fails())
            return $this->objResponse->Response(false, __FILE__, __LINE__);

        $search = [
            'column' => $request->all()['column'],
            'operator' => $request->all()['operator'],
            'value' => $request->all()['value']
        ];

        $validSearch = 0;
        foreach ($search as $key => $value)
            if ($key != 'value' && $value == null)
                $validSearch++;

        if ($validSearch != 0)
            $search = null;

        $data = $this->objDataManager->TableBody($request['currentpage'], $request['perpage'], $search);
        return $this->objResponse->Response(true, __FILE__, __LINE__, null, $data);
    }

    public function Update(Request $request)
    {

        if (!isset($this->objDataManager))
            return $this->objResponse->Response(false, __FILE__, __LINE__);

        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'column' => 'required',
            'value' => 'nullable'
        ]);

        if (!$validator->fails()) {

            $values = [$request->all()['column'] => $request->all()['value']];
            if ($this->objDataManager->Update($values, $request->all()['id']))
                $message = $this->objResponse->Message('msg', 'done');
            else
                $message = $this->objResponse->Message('msg', 'NO changes! :)');
        } else {

            $message = $this->objResponse->Message('msg', "<p>Amazing! something is wrong.</p>");
        }

        return $this->objResponse->Response(true, __FILE__, __LINE__, $message, null);
    }

    public function Create(Request $request)
    {

        if (!isset($this->objDataManager))
            return $this->objResponse->Response(false, __FILE__, __LINE__);

        $validator = Validator::make($request->all(), [
            'form' => 'required'
        ]);

        if (!$validator->fails()) {

            $lastId = $this->objDataManager->Create($request->all()['form']);
            $message = $this->objResponse->Message('msg', "<p>Your request done! ;)</p><p><strong>Last ID:</strong> {$lastId}</p>");
        } else {

            $message = $this->objResponse->Message('msg', "<p>Amazing something is wrong!</p>");
        }

        return $this->objResponse->Response(true, __FILE__, __LINE__, $message, null);
    }

    public function Delete(Request $request)
    {

        if (!isset($this->objDataManager))
            return $this->objResponse->Response(false, __FILE__, __LINE__);

        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if (!$validator->fails()) {

            $result = $this->objDataManager->Delete($request->all()['id']);
            if ($result == null)
                $message = $this->objResponse->Message('msg', "<p>Your request done! ;)</p>");
            else
                $message = $this->objResponse->Message('msg', "<p>Amazing! check \"ID\" again!</p>");
        } else {

            $message = $this->objResponse->Message('msg', "<p>Amazing something is wrong!</p>");
        }

        return $this->objResponse->Response(true, __FILE__, __LINE__, $message, null);
    }
}
