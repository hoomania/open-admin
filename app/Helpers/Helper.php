<?php

use Illuminate\Support\Arr;

if (!function_exists('setConnection')) {

    function setConnection($domain)
    {

        $objSites = new \App\Sites();

        $connection = $objSites->getDatabaseConnection($domain);
        config(Arr::dot($connection, 'database.connections.'));

        return \Illuminate\Support\Facades\DB::connection('mysql_guest');
    }
}

if (!function_exists('getGuestConnection')) {

    function getGuestConnection(){

        $guestConnection = 'mysql_guest';
        $connections = \Illuminate\Support\Facades\Config::get('database')['connections'];

        if (array_key_exists($guestConnection, $connections))
            return $guestConnection;

        return false;
    }
}


