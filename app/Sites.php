<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sites extends Model
{
    //

    public function IndexDomains()
    {
        $select = $this->select('domain')
            ->orderBy('domain')
            ->get();

        $domains = array();
        foreach ($select as $item)
            $domains[] = $item->domain;

        return $domains;
    }

    public function getDatabaseInfo($domain)
    {
        $select = $this->where('domain', $domain)->first();

        return [
            'database' => $select->database,
            'username' => $select->username,
            'password' => $select->password,
        ];
    }

    public function getDatabaseConnection($domain)
    {
        $select = $this->where('domain', $domain)->first();

        return [
            'mysql_guest' => [
                'driver' => 'mysql',
                'host' => env('DB_HOST', '127.0.0.1'),
                'port' => env('DB_PORT', '3306'),
                'database' => $select->database,
                'username' => $select->username,
                'password' => $select->password,
                'unix_socket' => env('DB_SOCKET', ''),
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => '',
                'strict' => true,
                'engine' => null,
            ]
        ];
    }

    public function getPath($domain)
    {
        $select = $this->where('domain', $domain)->first();

        return $select->path;
    }
}
