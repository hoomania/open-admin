<?php

namespace App\Classes;

use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class DataManager
{
    private $domain;
    private $DB;
    private $table;
    private $secureKey = [
        'username',
        'password'
    ];
    private $perPage = 20;
    private static $operators = [
        '=',
        '!=',
        'LIKE',
        'IS NULL',
        'IS NOT NULL',
        '<',
        '<=',
        '>',
        '>='
    ];

    public function __construct($domain, $table)
    {
        $this->DB = setConnection($domain)->table($table);
        $this->table = $table;
        $this->domain = $domain;
    }

    public static function getOperators()
    {
        return self::$operators;
    }

    public function TableBody($currentPage = 1, $perPage = null, array $search = null): array
    {
        $currentPage = $currentPage == null ? 1 : $currentPage;
        $perPage = $perPage == null ? $this->perPage : $perPage;

        if ($search == null) {

            $count = $this->DB->get()->count();
            $select = $this->DB;
        } else {

            $column = $search['column'];
            $condition = $search['operator'];
            $value = $search['value'];

            $count = $this->DB->where($column, $condition, $value)->get()->count();
            $select = $this->DB->where($column, $condition, $value);
        }

        if (($count % $perPage) == 0)
            $pages = (int)($count / $perPage);
        else
            $pages = (int)($count / $perPage) + 1;

        if ($currentPage < 1 || $currentPage > $pages)
            $currentPage = $pages;

        $data = Schema::connection(getGuestConnection())->getColumnListing($this->table);
        if (in_array('id', $data))
            $select = $select
                ->orderBy('id', 'desc')
                ->skip($perPage * ($currentPage - 1))
                ->take($perPage)
                ->get()
                ->toArray();
        else
            $select = $select
                ->skip($perPage * ($currentPage - 1))
                ->take($perPage)
                ->get()
                ->toArray();

        $body = "<table class='table table-striped'><thead class='thead-light'><tr>";

        $header = Schema::connection(getGuestConnection())->getColumnListing($this->table);
        $body .= "<th>Action</th>";
        foreach ($header as $item)
            $body .= "<th>$item</th>";

        $body .= "</tr></thead><tbody><tr>";
        foreach ($select as $item) {

            $body .= "<td><button type='button' class='btn btn-danger' 
                              data-domain='$this->domain' 
                              data-table='$this->table'
                              data-id='$item->id' 
                              data-toggle='modal' 
                              data-target='#modal_delete'
                              onClick='DeleteConfirm(this);'><i class='fas fa-trash'></i></button></td>";

            foreach ($item as $key => $value) {

                $break = false;

                if (!$break && in_array($key, $this->secureKey)) {

                    $value = "<button type='button' class='btn btn-success' 
                              data-domain='$this->domain' 
                              data-table='$this->table'
                              data-id='$item->id' 
                              data-column='$key'
                              onClick='ShowSecure(this);'><i class='fas fa-eye'></i></button>";

                    $body .= "<td>$value</td>";
                    $break = true;
                }

                if (!$break && $value != strip_tags($value)) {

                    $value = "<button type='button' class='btn btn-success' 
                              data-domain='$this->domain' 
                              data-table='$this->table'
                              data-id='$item->id' 
                              data-column='$key'
                              onClick='getColumnContent(this);'><i class='fas fa-code'></i></button>";

                    $body .= "<td>$value</td>";
                    $break = true;
                }


                if (!$break && strlen($value) > 100) {

                    $value = "<button type='button' class='btn btn-success' 
                              data-domain='$this->domain' 
                              data-table='$this->table'
                              data-id='$item->id' 
                              data-column='$key'
                              onClick='getColumnContent(this);'><i class='fas fa-ellipsis-h'></i></button>";

                    $body .= "<td>$value</td>";
                    $break = true;
                }

                if (!$break) {
                    $body .= "<td 
                          data-domain='$this->domain'
                          data-table='$this->table'
                          data-id='$item->id' 
                          data-column='$key'
                          data-toggle='modal' 
                          data-target='#modal_update'
                          onClick='UpdateColumnContent(this);'>$value</td>";
                }
            }

            $body .= "</tr>";
        }

        $body .= "</tbody></table>";

        return [
            'table' => [
                'header' => '',
                'body' => $body,
                'pagination' => $this->TablePagination($currentPage, $count, $perPage)
            ]
        ];
    }

    public function TablePagination($currentPage, $count, $perPage = null)
    {

        if ($perPage == null)
            $perPage = $this->perPage;
        if ($count == 0)
            return "<div class='col-sm-12'></div>";

        if (($count % $perPage) == 0)
            $pages = (int)($count / $perPage);
        else
            $pages = (int)($count / $perPage) + 1;

        if ($pages == 1)
            return "<div class='col-sm-12'></div>";

        if ($currentPage < 1 || $currentPage > $pages)
            $currentPage = $pages;

        $pageArray = [$currentPage];
        for ($i = 1; $i < 4; $i++) {

            if (($currentPage - $i) > 1)
                $pageArray[] = ($currentPage - $i);

            if (($currentPage + $i) < $pages)
                $pageArray[] = ($currentPage + $i);
        }

        sort($pageArray);

        $firstPage = 1;
        $lastPage = $pages;

        if (current($pageArray) == 2)
            $firstGap = "";
        else
            $firstGap = "<span><i class='fas fa-ellipsis-h'></i></span>";

        if (last($pageArray) == $pages - 1)
            $lastGap = "";
        else
            $lastGap = "<span><i class='fas fa-ellipsis-h'></i></span>";

        $html = "<div class='col-sm-12'>";

        if ($currentPage != 1)
            $html .= "
            <button type='button' class='btn-pagination btn btn-info' data-page='1' onclick='Pagination(this);'>
                    $firstPage
                </button>$firstGap";

        foreach ($pageArray as $item) {

            $number = $item;

            if ($item == $currentPage)
                $html .= "<button type='button' class='btn-pagination btn' data-page='$item' onclick='Pagination(this);'>
                    $number
                </button>";
            else
                $html .= "<button type='button' class='btn-pagination btn btn-info' data-page='$item' onclick='Pagination(this);'>
                    $number
                </button>";
        }

        if ($currentPage != $pages)
            $html .= "$lastGap<button type='button' class='btn-pagination btn btn-info' data-page='$pages' onclick='Pagination(this);'>
                    $lastPage
                 </button>";

        $html .= "</div>";

        return $html;
    }

    public function Create($data)
    {
        $id = $this->DB->insertGetId($data);

        return $id;
    }

    public function Read($column = null, $operator = '=', $value = null)
    {
        if ($column == null) {

            return $this->DB->get();
        }

        return $this->DB->where($column, $operator, $value)->get();
    }

    public function Update(array $data, $id): bool
    {
        $before = $this->DB->where('id', $id)->first();

        foreach ($data as $key => $value) {

            if ($before->$key != $value) {
                $this->DB->where('id', $id)->update($data);
                return true;
            }
        }

        return false;
    }

    public function Delete($id)
    {
        $this->DB->where('id', $id)->delete();

        return $this->DB->where('id', $id)->first();
    }

    public function FormMaker($database, $table): string
    {
        $columns = DB::table('information_schema.columns')
            ->select('column_name', 'column_default', 'is_nullable', 'column_type', 'extra')
            ->where([
                ['table_schema', $database],
                ['table_name', $table],
            ])->get();

        $columns = json_decode(json_encode($columns), True);
        $form = '';
        foreach ($columns as $column) {

            $is_nullable = 'nullable: ' . strtolower($column['IS_NULLABLE']);
            $value = $column['COLUMN_DEFAULT'] == null ? '' : $column['COLUMN_DEFAULT'];

            $form .= "<div class='form-group'>
            <label for='{$column['COLUMN_NAME']}' class='col-form-label'>
            <strong><span style='color: #28a745'>{$column['COLUMN_NAME']}</span></strong>
            <span style='color: #999999'>| Type: {$column['COLUMN_TYPE']} </span></label>
            <input type='text' class='w-100' name='{$column['COLUMN_NAME']}' id='{$column['COLUMN_NAME']}' placeholder='{$is_nullable}' value='{$value}'>
            </div>";
        }

        return $form;
    }
}