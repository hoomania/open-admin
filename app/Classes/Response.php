<?php
/**
 * Created by PhpStorm.
 * User: hooman
 * Date: 01/09/19
 * Time: 11:00 AM
 */

namespace App\Classes;

class Response
{

    private $prefix = 'f-';
    private $fileNames;
    private $messageTitle = [
        'msg' => 'Message'
    ];
    private $messageText = [
        'done' => 'Your request done! ;)'
    ];

    public function __construct()
    {

        $this->fileNames = [
            'AjaxDataManagerController' => $this->prefix . '001',
            'AjaxLoginController' => $this->prefix . '002',
            'AjaxSelectController' => $this->prefix . '003',
            'AjaxUploaderController' => $this->prefix . '004'
        ];
    }

    public function Response($status, $file, $line, $message = null, $data = null)
    {

        $fileName = basename($file, '.php');

        if (!array_key_exists($fileName, $this->fileNames))
            return [
                'result' => $status,
                'code' => 'f-xx-' . $line
            ];

        $result = [
            'header' => [
                'result' => $status,
                'code' => $this->fileNames[$fileName] . '-' . $line
            ],
            'body' => [
                'message' => $message,
                'data' => $data
            ]
        ];

        return response()->json($result);
    }

    public function Message($title, $text, $backDrop = true, $category = 'alert', $type = null)
    {

        ////INFO: category: alert, confirm, prompt
        ////INFO: type: success, warning, info

        $title = $this->messageTitle[$title];
        if (key_exists($text, $this->messageText))
            $text = $this->messageText[$text];

        return $message = [
            'backdrop' => $backDrop,
            'category' => $category,
            'text' => $text,
            'title' => $title,
            'type' => $type
        ];
    }
}