<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Admin Panel :: Dashboard</title>

    @include('admin.lib_style')
    @yield('custom-lib-style')
    @yield('custom-style')

</head>
<body>

@yield('content')

@include('admin.lib_js')
@yield('custom-lib-js')
@yield('custom-js')

</body>
</html>
