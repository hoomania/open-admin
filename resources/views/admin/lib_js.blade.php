<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/js/all.min.js"></script>

<script src="{{ asset('packages/codemirror/lib/codemirror.js') }}"></script>
<script src="{{ asset('packages/codemirror/mode/xml/xml.js') }}"></script>
<script src="{{ asset('packages/codemirror/mode/css/css.js') }}"></script>
<script src="{{ asset('packages/codemirror/mode/javascript/javascript.js') }}"></script>
<script src="{{ asset('packages/codemirror/mode/htmlmixed/htmlmixed.js') }}"></script>
<script src="{{ asset('packages/codemirror/addon/selection/active-line.js') }}"></script>
<script src="{{ asset('js/serializetojson.js') }}"></script>
<script src="{{ asset('js/admin.js?v=0.1') }}"></script>
