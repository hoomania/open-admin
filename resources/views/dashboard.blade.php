@extends('admin.masterpage')

@section('content')
    <input type="hidden" id="ajax_path" value="ajax/">
    <br>
    <div class="container-fluid">
        <a class="btn btn-primary" href="/admin/logout">
            Logout
        </a>
        <br />
        <br />
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <h5 class="card-header">Table</h5>
                    <div class="card-body">
                        <div class="form-inline">
                            <div class="col-md-2">
                                <label for="select_sites" class="col-form-label"></label>
                                <select class="w-100" id="select_sites">
                                    <option value="" disabled selected>Select Site</option>
                                    @foreach($domains as $item)
                                        <option value="{{$item}}">{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label for="select_tables" class="col-form-label"></label>
                                <select class="w-100" id="select_tables">
                                    <option value="" disabled selected>Select Table</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label for="select_column" class="col-form-label"></label>
                                <select class="w-100" id="select_column">
                                    <option value="" disabled selected>Column</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label for="select_operator" class="col-form-label"></label>
                                <select class="w-100" id="select_operator">
                                    <option value="" disabled selected>Operator</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label for="search_value" class="col-form-label"></label>
                                <input type="text" class="w-100" id="search_value" placeholder="Value"/>
                            </div>
                            <div class="col-md-2 btn-group" style="padding: 14px 0 0 0;">
                                    <div class="form-group">
                                        <label for="button_search" class="col-form-label"></label>
                                        <button id="button_search" class="btn btn-primary" onclick="Search();">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                    <div class="form-group">
                                        <label for="button_refresh" class="col-form-label"></label>
                                        <button id="button_refresh" class="btn btn-primary" onclick="ForceRefresh();">
                                            <i class="fas fa-sync"></i>
                                        </button>
                                    </div>
                                    <div class="form-group">
                                        <label for="button_form" class="col-form-label"></label>
                                        <button id="button_form" type='button' class='btn btn-success'
                                                onclick="FormMaker();">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <h5 class="card-header">Content</h5>
                    <div class="card-body">
                        <div class="col-md-12" id="table_body" style="overflow-x: scroll;">

                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-md-9" id="table_pagination"></div>
                            <div class="col-md-3 form-inline">
                                <div class="form-group">
                                    <label for="select_perpage" class="col-form-label">Number of rows: </label>&nbsp;
                                    <select id="select_perpage">
                                        <option value="20" selected>20</option>
                                        <option value="10">10</option>
                                        <option value="15">15</option>
                                        <option value="30">30</option>
                                        <option value="40">40</option>
                                        <option value="50">50</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-8">
                <div class="card" id="bookmark_editor">
                    <h5 class="card-header">Code</h5>
                    <div class="card-body">
                        <textarea class="codemirror-textarea" id="code_editor" cols="30" rows="10"
                                  style="width: 100%;"></textarea>
                    </div>
                    <div class="card-footer">
                        <button id="button_update_editor" type='button' class='btn btn-success'
                                onclick="UpdateFromEditor();">
                            <i class="far fa-save"></i>
                        </button>
                        <button id="button_format" type='button' class='btn btn-success' onclick="EditorAutoFormat();">
                            <i class="fas fa-align-left"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <h5 class="card-header">File Uploader</h5>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="select_filename" class="col-form-label">File Name Pattern</label>
                            <select class="w-100" id="select_filename">
                                <option value="-1" disabled="">Select Filename Pattern</option>
                            </select>
                            <div id="div_custom_filename">
                                <label for="file_name" class="col-form-label">Custom File Name</label>
                                <br>
                                <input type="text" class="w-100" id="file_name"/>
                            </div>
                            <br>
                            <label for="select_directory" class="col-form-label">Directory</label>
                            <select class="w-100" id="select_directory">
                                <option value="-1" disabled="">Select Directory</option>
                            </select>

                        </div>
                    </div>
                    <div class="card-footer">
                        <button id="button_browse" type='button' class='btn btn-success'>
                            <i class="fas fa-file-upload"></i>
                        </button>
                        <button id="button_add_directory" type='button' class='btn btn-success'>
                            <i class="fas fa-folder-plus"></i>
                        </button>
                        <input id="upload_file" type="file" accept="image/jpeg" style="display: none;"/>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_update" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <span id="target_domain"></span><br>
                    <span id="target_table"></span><br>
                    <span id="target_id"></span><br>
                    <span id="target_column"></span><br><br>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Content:</label>
                        <textarea class="form-control" id="column_content"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        <i class="fas fa-times"></i>
                    </button>
                    <button id="button_update" type="button" class="btn btn-success" onclick="Update();">
                        <i class="fas fa-check"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_create" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Create</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="form_create">

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        <i class="fas fa-times"></i>
                    </button>
                    <button id="button_create" type="button" class="btn btn-success" onclick="Create(this);">
                        <i class="fas fa-check"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_delete" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>
                        <strong>Are you sure?!</strong>
                    </p>
                    <span id="target_delete_domain"></span><br>
                    <span id="target_delete_table"></span><br>
                    <span id="target_delete_id"></span><br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">
                        <i class="fas fa-times"></i>
                    </button>
                    <button id="button_delete" type="button" class="btn btn-danger" onclick="Delete();">
                        <i class="fas fa-check"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
@stop

{{--@section('custom-js')--}}
{{--    <script type="text/javascript">--}}

{{--        $(function () {--}}

{{--        });--}}
{{--    </script>--}}
{{--@stop--}}
