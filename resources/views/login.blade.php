<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <title>Open Admin</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css">

    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
{{--    <link rel="stylesheet" href="{{ asset('packages/particles/css/style.css') }}">--}}
</head>
<body>
<input type="hidden" id="ajax_path" value="login/">
{{--<div id="particles-js" style="height: 100vh;">--}}
{{--    <div style="position:absolute; width: 100%; height: 100%; background: #ffffff00;">--}}
        <div class="login-form">
            <div class="form">
                <h2 class="text-center">Open Admin</h2>
                <div class="form-group">
                    <label for="username"></label>
                    <input id="username" type="text" class="form-control" placeholder="Username">
                </div>
                <div class="form-group">
                    <label for="password"></label>
                    <input id="password" type="password" class="form-control" placeholder="Password">
                </div>
                <div class="form-group">
                    <button id="button_login"
                            class="btn btn-primary btn-lg btn-block"
                            onclick="Login()">
                        Sign In
                    </button>
                </div>
                <br>
                <div class="man-dordad">
                    <span> Made with <i class="fa fa-heart pulse man-heart"></i> in Kerman | <a href="http://dordad.com" target="_blank">dordad.com</a></span>
                </div>
            </div>
        </div>
{{--    </div>--}}
{{--</div>--}}

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/js/all.min.js"></script>

{{--<script src="{{asset('/packages/particles/js/particles.js')}}"></script>--}}
{{--<script src="{{asset('/packages/particles/js/app.js')}}"></script>--}}
<script src="{{asset('/js/login.js')}}"></script>

</body>
</html>