<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/admin/login', 'LoginController@Index')->name('login');
Route::get('/admin/logout', 'LoginController@Logout')->name('logout');
Route::post('/admin/login/login', 'AjaxLoginController@Login');

Route::group(['middleware' => ['auth', 'web']], function () {

    Route::get('/admin/dashboard', 'DashboardController@Index');

    Route::post('/admin/ajax/get-select-tables', 'AjaxSelectController@getSelectTables');
    Route::post('/admin/ajax/get-select-columns', 'AjaxSelectController@getSelectColumns');
    Route::post('/admin/ajax/get-select-operators', 'AjaxSelectController@getSelectOperators');

    Route::post('/admin/ajax/get-table', 'AjaxDataManagerController@getTable');
    Route::post('/admin/ajax/get-column-content', 'AjaxDataManagerController@getColumnContent');

    Route::post('/admin/ajax/read', 'AjaxDataManagerController@Read');
    Route::post('/admin/ajax/create', 'AjaxDataManagerController@Create');
    Route::post('/admin/ajax/update', 'AjaxDataManagerController@Update');
    Route::post('/admin/ajax/delete', 'AjaxDataManagerController@Delete');
    Route::post('/admin/ajax/form-maker', 'AjaxDataManagerController@FormMaker');
    Route::post('/admin/ajax/show-secure-content', 'AjaxDataManagerController@getColumnContent');
});


