var editor;
var dataTransferKey = {
  'domain': null,
  'table': null,
  'currentpage': null,
  'perpage': null,
  'search': {
    'column': null,
    'operator': null,
    'value': null
  }
};

var dataCUD = {
  'domain': null,
  'table': null,
  'id': null,
  'column': null
};

var arrayCallback = {
  'get-select-tables': ['FillSelectTables(xhr)'],
  'get-select-columns': ['FillSelectColumn(xhr)'],
  'get-select-operators': ['FillSelectOperator(xhr)'],
  'get-table': ['FillTable(xhr)'],
  'get-column-content': ['FillCodeEditor(xhr)'],
  'form-maker': ['FillFormModal(xhr)'],
  'show-secure-content': ['UpdateSecureContent(xhr)'],
  'create': ['ShowMessage(xhr)', 'Refresh(xhr)', 'CloseModal("modal_create")'],
  'read': ['FillTable(xhr)'],
  'update': ['ShowMessage(xhr)', 'Refresh(xhr)', 'CloseModal("modal_update")'],
  'delete': ['ShowMessage(xhr)', 'Refresh(xhr)', 'CloseModal("modal_delete")']
};

function AjaxRequest(define, data) {

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $.ajax({
    url: $('#ajax_path').val() + define,
    data: data,
    type: 'POST',
    dataType: 'JSON',
    // contentType: false,
    // processData: false,
    success: function(xhr, response) {

      $.each(arrayCallback, function(index, value) {

        if (index === define)
          for (var i = 0; i < value.length; i++) {

            eval(value[i]);
          }
      });
    },
    error: function(xhr, status, error) {

      alert(xhr.responseText);
    }
  });
}

function ShowMessage(inputJson) {

  if (!inputJson.body['message'])
    return;

  switch (inputJson.body.message['category']) {

    case 'alert':

      bootbox.alert({
        size: 'small',
        title: inputJson.body.message['title'],
        message: inputJson.body.message['text'],
        backdrop: inputJson.body.message['backdrop'],
        buttons: {
          ok: {
            label: '<i class="far fa-smile"></i>',
            className: 'btn-success'
          }
        }
      });
      break;

    case 'confirm':
      break;

    case 'prompt':
      break;

    default:
      break;
  }
}

function Update() {

  var data = {
    'domain': dataCUD['domain'],
    'table': dataCUD['table'],
    'id': dataCUD['id'],
    'column': dataCUD['column'],
    'value': $('#column_content').val()
  };

  AjaxRequest('update', data);
}

function Create() {

  var obj = $("#form_create").serializeToJSON({

    // serialize the form using the Associative Arrays
    associativeArrays: true,

    // convert "true" and "false" to booleans true / false
    parseBooleans: true,
    parseFloat: {

      // the value can be a string or function
      condition: undefined,

      // auto detect NaN value and changes the value to zero
      nanToZero: true,

      // return the input value without commas
      getInputValue: function($input) {
        return $input.val().split(",").join("");
      }
    }

  });

  var data = {
    'domain': dataCUD['domain'],
    'table': dataCUD['table'],
    'form': obj
  };

  AjaxRequest('create', data);
}

function Delete() {

  var data = {
    'domain': dataCUD['domain'],
    'table': dataCUD['table'],
    'id': dataCUD['id']
  };

  AjaxRequest('delete', data);
}

function UpdateFromEditor() {

  var data = {
    'domain': dataCUD['domain'],
    'table': dataCUD['table'],
    'id': dataCUD['id'],
    'column': dataCUD['column'],
    'value': editor.getValue()
  };

  AjaxRequest('update', data);
}

function Search() {

  var data = {
    'domain': dataTransferKey['domain'],
    'table': dataTransferKey['table'],
    'column': dataTransferKey.search['column'],
    'operator': dataTransferKey.search['operator'],
    'value': dataTransferKey.search['value'],
    'currentpage': dataTransferKey['currentpage'],
    'perpage': dataTransferKey['perpage']
  };

  AjaxRequest('read', data);
}

function Refresh() {

  //ResetPage('refresh');
  if (dataTransferKey.search['column'] != null &&
    dataTransferKey.search['operator'] != null &&
    dataTransferKey.search['value'] != null) {

    Search();
  } else {

    var data = {
      'domain': dataTransferKey['domain'],
      'table': dataTransferKey['table'],
      'column': dataTransferKey.search['column'],
      'operator': dataTransferKey.search['operator'],
      'value': dataTransferKey.search['value'],
      'currentpage': dataTransferKey['currentpage'],
      'perpage': dataTransferKey['perpage']
    };

    AjaxRequest('get-table', data);
  }
}

function ForceRefresh() {

  var data = {
    'domain': dataTransferKey['domain'],
    'table': dataTransferKey['table'],
    'column': dataTransferKey.search['column'],
    'operator': dataTransferKey.search['operator'],
    'value': dataTransferKey.search['value'],
    'currentpage': dataTransferKey['currentpage'],
    'perpage': dataTransferKey['perpage']
  };

  AjaxRequest('get-table', data);
}

function FormMaker() {

  var data = {
    'domain': dataTransferKey['domain'],
    'table': dataTransferKey['table']
  };

  AjaxRequest('form-maker', data);
}

function Pagination(element) {

  dataTransferKey['currentpage'] = $(element).data('page');
  Search();
}

function CloseModal(element) {

  $('#' + element).modal('hide');

  dataCUD = {
    'domain': null,
    'table': null,
    'id': null,
    'column': null
  };
}

function FillSelectTables(xhr) {

  var res = $(xhr.body['data']).map(function() {

    return '<option value="' + this + '">' + this + '</option>';
  });

  $.each(res, function(index, value) {

    $('#select_tables').append(value);
  });
}

function FillTable(xhr) {

  $('#table_body').empty().html(xhr.body.data.table['body']);
  $('#table_pagination').empty().html(xhr.body.data.table['pagination']);
}

function FillCodeEditor(xhr) {

  if (xhr.header['result']) {

    var identity = xhr.body.data['identity'];

    dataCUD['domain'] = identity['domain'];
    dataCUD['table'] = identity['table'];
    dataCUD['id'] = identity['id'];
    dataCUD['column'] = identity['column'];

    editor.setValue(xhr.body.data['content']);

    $([document.documentElement, document.body]).animate({
      scrollTop: $("#bookmark_editor").offset().top
    }, 1000);
  }
}

function FillSelectFilename(xhr) {

  var res = $(xhr.body['data']).map(function() {

    return '<option value="' + this + '">' + this + '</option>';
  });

  $.each(res, function(index, value) {

    $('#select_filename').append(value);
  });
}

function FillSelectDirectory(xhr) {

  var res = $(xhr.body['data']).map(function() {

    return '<option value="' + this + '">' + this + '</option>';
  });

  $.each(res, function(index, value) {

    $('#select_directory').append(value);
  });
}

function FillSelectColumn(xhr) {

  var res = $(xhr.body['data']).map(function() {

    return '<option value="' + this + '">' + this + '</option>';
  });

  $.each(res, function(index, value) {

    $('#select_column').append(value);
  });
}

function FillSelectOperator(xhr) {

  var res = $(xhr.body['data']).map(function() {

    return '<option value="' + this + '">' + this + '</option>';
  });

  $.each(res, function(index, value) {

    $('#select_operator').append(value);
  });
}

function FillFormModal(xhr) {

  if (xhr.body['data'] !== null) {
    $('#form_create').empty().html(xhr.body['data']);
    $('#modal_create').modal('show');

    dataCUD['domain'] = dataTransferKey['domain'];
    dataCUD['table'] = dataTransferKey['table'];
  }
}

function getColumnContent(element) {

  var data = {
    'domain': $(element).data('domain'),
    'table': $(element).data('table'),
    'id': $(element).data('id'),
    'column': $(element).data('column')
  };

  AjaxRequest('get-column-content', data);
}

function UpdateColumnContent(element) {

  $('#column_content').val($(element).html());
  $('#target_domain').html('<strong>Domain: </strong>' + $(element).data('domain'));
  $('#target_table').html('<strong>Table: </strong>' + $(element).data('table'));
  $('#target_id').html('<strong>ID: </strong>' + $(element).data('id'));
  $('#target_column').html('<strong>Column: </strong>' + $(element).data('column'));

  dataCUD['domain'] = $(element).data('domain');
  dataCUD['table'] = $(element).data('table');
  dataCUD['id'] = $(element).data('id');
  dataCUD['column'] = $(element).data('column');
}

function ShowSecure(element) {

  dataCUD['domain'] = $(element).data('domain');
  dataCUD['table'] = $(element).data('table');
  dataCUD['id'] = $(element).data('id');
  dataCUD['column'] = $(element).data('column');

  AjaxRequest('show-secure-content', dataCUD)
}

function UpdateSecureContent(xhr) {

  $('#column_content').val(xhr.body.data['content']);
  $('#target_domain').html('<strong>Domain: </strong>' + xhr.body.data.identity['domain']);
  $('#target_table').html('<strong>Table: </strong>' + xhr.body.data.identity['table']);
  $('#target_id').html('<strong>ID: </strong>' + xhr.body.data.identity['id']);
  $('#target_column').html('<strong>Column: </strong>' + xhr.body.data.identity['column']);
  //
  dataCUD['domain'] = xhr.body.data.identity['domain'];
  dataCUD['table'] = xhr.body.data.identity['table'];
  dataCUD['id'] = xhr.body.data.identity['id'];
  dataCUD['column'] = xhr.body.data.identity['column'];

  $('#modal_update').modal('toggle');
}

function DeleteConfirm(element) {

  $('#target_delete_domain').html('<strong>Domain: </strong>' + $(element).data('domain'));
  $('#target_delete_table').html('<strong>Table: </strong>' + $(element).data('table'));
  $('#target_delete_id').html('<strong>ID: </strong>' + $(element).data('id'));

  dataCUD['domain'] = $(element).data('domain');
  dataCUD['table'] = $(element).data('table');
  dataCUD['id'] = $(element).data('id');
}

function EditorAutoFormat() {

  for (var i = 0; i < editor.lineCount(); i++) {
    editor.indentLine(i);
  }
}

function ResetPage(level) {

  switch (level) {

    case 'domain':

      dataTransferKey = {
        'domain': null,
        'table': null,
        'currentpage': null,
        'perpage': null,
        'search': {
          'column': null,
          'operator': null,
          'value': null
        }
      };

      $('#table_body').empty();
      $('#table_pagination').empty();
      $("#select_tables option[value!='']").remove();
      $("#select_column option[value!='']").remove();
      $("#select_operator option[value!='']").remove();
      $("#select_tables").val('');
      $("#select_column").val('');
      $("#select_operator").val('');
      $("#search_value").val('');
      editor.setValue('');
      break;

    case 'table':

      dataTransferKey['currentpage'] = null;
      dataTransferKey.search['column'] = null;
      dataTransferKey.search['operator'] = null;
      dataTransferKey.search['value'] = null;

      $('#table_body').empty();
      $('#table_pagination').empty();
      $("#select_column option[value!='']").remove();
      $("#select_operator option[value!='']").remove();
      $("#select_column").val('');
      $("#select_operator").val('');
      $("#search_value").val('');
      editor.setValue('');
      break;

    case 'refresh':

      dataTransferKey['currentpage'] = null;
      dataTransferKey.search['column'] = null;
      dataTransferKey.search['operator'] = null;
      dataTransferKey.search['value'] = null;

      $('#table_body').empty();
      $('#table_pagination').empty();
      $("#select_column").val('');
      $("#select_operator").val('');
      $("#search_value").val('');
      $('#select_perpage').val('4');
      editor.setValue('');
      break;

    default:
      break;
  }
}

$("#select_sites").change(function() {

  var domain = $(this).children("option:selected").val();

  ResetPage('domain');
  dataTransferKey['domain'] = domain;

  var data = {
    'domain': domain
  };
  AjaxRequest('get-select-tables', data);
});

$("#select_tables").change(function() {

  ResetPage('table');

  var domain = dataTransferKey['domain'];
  var table = $(this).children("option:selected").val();

  dataTransferKey['table'] = table;

  var data = {
    'domain': domain,
    'table': table
  };

  AjaxRequest('get-select-columns', data);
  AjaxRequest('get-select-operators', data);
  AjaxRequest('get-table', data);
});

$("#select_column").change(function() {

  dataTransferKey.search['column'] = $(this).children("option:selected").val();
});

$("#select_operator").change(function() {

  dataTransferKey.search['operator'] = $(this).children("option:selected").val();
});

$("#select_perpage").change(function() {

  dataTransferKey['perpage'] = $(this).children("option:selected").val();
  Search();
});

$("#search_value").on("change paste keyup", function() {

  dataTransferKey.search['value'] = $(this).val();
});

$("#select_filename").change(function() {

  var pattern = $(this).children("option:selected").val();

  if (pattern === 'custom')
    $('#div_custom_filename').css('display', 'block');
  else
    $('#div_custom_filename').css('display', 'none');
});

$('#button_browse').click(function() {

  $('#upload_file').click();
});

$('#upload_file').change(function(e) {

  alert('nothing to do! :)');
});

$(document).ready(function() {

  var code = $('.codemirror-textarea')[0];
  editor = CodeMirror.fromTextArea(code, {
    lineNumbers: true,
    mode: "text/html",
    styleActiveLine: true
  })
});
