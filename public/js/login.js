var arrayCallback = {
    'login': ['LoginResponse(xhr)']
};

function AjaxRequest(define, data) {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            // 'Authorization-Custom': btoa($('input[name="username"]').val() + ':' + $('input[name="password"]').val())
        }
    });

    $.ajax({
        url: $('#ajax_path').val() + define,
        data: data,
        type: 'POST',
        dataType: 'JSON',
        success: function (xhr, response) {

            $.each(arrayCallback, function (index, value) {

                if (index === define)
                    for (var i = 0; i < value.length; i++) {

                        eval(value[i]);
                    }
            });
        },
        error: function (xhr, status, error) {

            console.log(xhr.responseText);
        }
    });
}

function LoginResponse(xhr) {

    switch (xhr.header['result']) {

        case true:
            location.replace(xhr.body['data']);
            break;

        case false:
            ShowMessage(xhr);
            break;

        default:
            break;
    }
}

function ShowMessage(inputJson) {

    if (!inputJson.body['message'])
        return;

    switch (inputJson.body.message['category']) {

        case 'alert':

            bootbox.alert({
                size: 'small',
                title: inputJson.body.message['title'],
                message: inputJson.body.message['text'],
                backdrop: inputJson.body.message['backdrop'],
                buttons: {
                    ok: {
                        label: '<i class="far fa-smile"></i>',
                        className: 'btn-success'
                    }
                }
            });
            break;

        default:
            break;
    }
}

function Login(){

    var data = {
        'username': $('#username').val(),
        'password': btoa($('#password').val()),
    };

    AjaxRequest('login', data);
}

